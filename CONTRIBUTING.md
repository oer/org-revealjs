<!--- Local IspellDict: en -->
<!-- Copyright (C) 2019 Jens Lechtenbörger -->
<!-- SPDX-License-Identifier: GPL-3.0-or-later -->

Your contributions in the form of patches or merge/pull requests are
very welcome!

Note that this project with all its contents is published under the
GNU General Public License, version 3 or later.  Thus, when submitting
a contribution you agree that your work is published under that same
license.  Also, when submitting contributions you assert that you hold
the necessary rights (which typically means that the contribution is
your original piece of work, but not something belonging to, say, your
employer or to some other author).

At least in Germany, copyright always remains with the author.
However, the amount of work that is necessary for copyright laws to be
applicable is not clearly defined.  Thus, I suggest that with
non-trivial contributions you include a line as follows:

```
;; Copyright (C) <year> <your name> <your e-mail address if you like>
```
